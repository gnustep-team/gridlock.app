//
//  AtomicViewDelegate.m
//  Gridlock
//
//  Created by Brian on Fri Feb 13 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "AtomicViewDelegate.h"
#import "ImageStore.h"
#import "AtomicGame.h"

@implementation AtomicViewDelegate

-(void)drawPieceImageForPlayer:(int)pnum times:(int)numImages inRect:(NSRect)rect {
  NSImage *image;
  float scale = 1.0f/3;
  
  image = [[ImageStore defaultStore] pieceImageForPlayer:pnum 
                                                withSize:NSMakeSize(rect.size.width*scale, rect.size.height*scale)];
  
  if (numImages==1) {
    float offset = (1-scale)/2;
    [image compositeToPoint:NSMakePoint(rect.origin.x + offset*rect.size.width, rect.origin.y + offset*rect.size.height)
                  operation:NSCompositeSourceOver];
  }
  if (numImages==2) {
    float yoff = (1-scale)/2;
    float xgap = (1-2*scale)/3;
    [image compositeToPoint:NSMakePoint(rect.origin.x + xgap*rect.size.width, rect.origin.y + yoff*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + (scale+2*xgap)*rect.size.width, rect.origin.y + yoff*rect.size.height)
                  operation:NSCompositeSourceOver];    
  }
  if (numImages==3) {
    float coff = (1-scale)/2;
    float gap = (1-2*scale)/3;
    [image compositeToPoint:NSMakePoint(rect.origin.x + gap*rect.size.width, rect.origin.y + gap*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + (scale+2*gap)*rect.size.width, rect.origin.y + gap*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + coff*rect.size.width, rect.origin.y + (scale+2*gap)*rect.size.height)
                  operation:NSCompositeSourceOver];    
  }
  if (numImages>3) {
    float gap = (1-2*scale)/3;
    [image compositeToPoint:NSMakePoint(rect.origin.x + gap*rect.size.width, rect.origin.y + gap*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + (scale+2*gap)*rect.size.width, rect.origin.y + gap*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + gap*rect.size.width, rect.origin.y + (scale+2*gap)*rect.size.height)
                  operation:NSCompositeSourceOver];    
    [image compositeToPoint:NSMakePoint(rect.origin.x + (scale+2*gap)*rect.size.width, rect.origin.y + (scale+2*gap)*rect.size.height)
                  operation:NSCompositeSourceOver];    
    
  }
}

-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(AtomicGame *)game {
  int owner = [game ownerOfRow:row column:col];
  if (owner>0) {
    [self drawPieceImageForPlayer:owner times:abs([game valueAtRow:row column:col]) inRect:rect];
  }
  return YES;
}

@end
