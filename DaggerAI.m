//
//  DaggerAI.m
//  Gridlock
//
//  Created by Brian on Sun Feb 29 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "DaggerAI.h"
#import "DaggerGame.h"

@implementation DaggerAI

-(id)init {
  self = [super init];
  p1DaggerWeight = 20;
  p2DaggerWeight = 30;
  crownPositionWeight = 5;
  return self;
}

-(int)utilityForGame:(DaggerGame *)game player:(int)pnum {
  int daggerWeight = (pnum==1) ? p1DaggerWeight : p2DaggerWeight;
  int baserow = (pnum==1) ? 0 : [game numberOfRows]-1;
  int utility = daggerWeight*[game numberOfDaggersForPlayer:pnum];
  NSEnumerator *ce = [[game grid] enumeratorForPositionsWithValue:-pnum];
  id crownPos;
  while (crownPos=[ce nextObject]) {
    utility += crownPositionWeight * abs([crownPos row]-baserow);
  }
  return utility;
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self utilityForGame:game player:pnum] - [self utilityForGame:game player:[game playerNumberMovingAfterPlayer:pnum]];
}

@end
