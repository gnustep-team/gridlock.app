/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "MyWindow.h"

/** For some reason, after the BoardView is resized, clicks with a height greater than the original
height of the BoardView don't get sent to it. This NSWindow subclass overrides -mouseDown to
dispatch clicks to the BoardView if the click falls within it.
*/
@implementation MyWindow

-(void)awakeFromNib {
  NSEnumerator *ve = [[[self contentView] subviews] objectEnumerator];
  id view;
  while (!boardView && (view=[ve nextObject])) {
    if ([NSStringFromClass([view class]) isEqualToString:@"BoardView"]) {
      boardView = view;
    }
  }
}

-(void)mouseDown:(NSEvent *)event {
  if (boardView && [boardView hitTest:[event locationInWindow]]==boardView) {
    [boardView mouseDown:event];
  }
  else {
    [super mouseDown:event];
  }
}

@end
