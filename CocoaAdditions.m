/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "CocoaAdditions.h"

static NSComparisonResult keyCompareFunc(id obj1, id obj2, void *context) {
  NSString *key = (NSString *)context;
  return [[obj1 valueForKey:key] compare:[obj2 valueForKey:key]];
}

static NSComparisonResult countCompareFunc(id obj1, id obj2, void *context) {
  int c1 = [obj1 count];
  int c2 = [obj2 count];
  if (c1==c2) return NSOrderedSame;
  return (c1>c2) ? NSOrderedDescending : NSOrderedAscending;
}

@implementation NSObject (DCArchivingAdditions)

-(NSData *)archivedData_ {
  return [NSArchiver archivedDataWithRootObject:self];
}

+(id)objectWithArchivedData_:(NSData *)data {
  return [NSUnarchiver unarchiveObjectWithData:data];
}

@end

@implementation NSUserDefaults (DCArchivingAdditions)

-(id)archivedObjectForKey:(id)key {
  return [NSUnarchiver unarchiveObjectWithData:[self objectForKey:key]];
}

-(void)setArchivedObject:(id)obj forKey:(id)key {
  [self setObject:[obj archivedData_] forKey:key];
  
}
@end

@implementation NSArray (DCMiscAdditions)

-(id)objectWithValue:(id)searchValue forSelector:(SEL)selector withArgument:(id)arg {
  NSEnumerator *e = [self objectEnumerator];
  id obj;
  while (obj=[e nextObject]) {
    id value = [obj performSelector:selector withObject:arg];
    if (value==nil && searchValue==nil) return obj;
    if ([value isEqual:searchValue]) return obj;
  }
  return nil;
}

-(NSArray *)arrayByIntersectingArray_:(NSArray *)array {
  NSEnumerator *e = [self objectEnumerator];
  NSMutableArray *intersection = [NSMutableArray array];
  id obj;
  while (obj=[e nextObject]) {
    if ([array containsObject:obj]) {
      [intersection addObject:obj];
    }
  }
  return intersection;
}

-(NSArray *)arrayByRemovingObjectsFromArray_:(NSArray *)array {
  NSMutableArray *result = [NSMutableArray array];
  NSEnumerator *e = [self objectEnumerator];
  id obj;
  while (obj=[e nextObject]) {
    if (![array containsObject:obj]) {
      [result addObject:obj];
    }
  }
  return result;
}

-(NSArray *)valuesByObjectsPerformingSelector:(SEL)sel {
  NSMutableArray *result = [NSMutableArray array];
  NSEnumerator *e = [self objectEnumerator];
  id obj;
  while (obj=[e nextObject]) {
    [result addObject:[obj performSelector:sel]];
  }
  return result;
}

-(NSArray *)valuesByObjectsPerformingSelector:(SEL)sel withObject_:(id)arg {
  NSMutableArray *result = [NSMutableArray array];
  NSEnumerator *e = [self objectEnumerator];
  id obj;
  while (obj=[e nextObject]) {
    [result addObject:[obj performSelector:sel withObject:arg]];
  }
  return result;  
}

-(NSArray *)arrayWithPrefix_:(NSArray *)prefix {
  if ([prefix count]==0) return NO;
  NSEnumerator *e = [self objectEnumerator];
  id obj;
  while (obj=[e nextObject]) {
    if ([obj isKindOfClass:[NSArray class]]) {
      if ([prefix count]<[obj count] &&
          [prefix isEqual:[obj subarrayWithRange:NSMakeRange(0,[prefix count])]]) {
        return obj;
      }
    }
  }
  return nil;
}

-(NSArray *)arrayWithObjectsInRandomOrder_ {
  NSMutableArray *array = [NSMutableArray arrayWithArray:self];
  [array randomizeObjects_];
  return array;
}

-(NSArray *)sortedArrayUsingKey_:(NSString *)key {
  return [self sortedArrayUsingFunction:keyCompareFunc context:key];
}

-(NSArray *)sortedArrayByCount_ {
  return [self sortedArrayUsingFunction:countCompareFunc context:NULL];
}

@end

@implementation NSMutableArray (DCMiscAdditions)

-(void)randomizeObjects_ {
  int count = [self count];
  int i;
  for(i=0; i<count-1; i++) {
    int incr = random() % (count-i);
    if (incr>0) [self exchangeObjectAtIndex:i withObjectAtIndex:i+incr];
  }
}

-(void)sortArrayUsingKey_:(NSString *)key {
  [self sortUsingFunction:keyCompareFunc context:key];
}

-(void)sortArrayByCount_ {
  [self sortUsingFunction:countCompareFunc context:NULL];
}

@end


@implementation NSPopUpButton (DCAppKitAdditions)
-(void)setItemTitles:(NSArray *)titles representedObjects_:(NSArray *)objects {
  int i,len=[titles count];
  [self removeAllItems];
  for(i=0; i<len; i++) {
    [self addItemWithTitle:[titles objectAtIndex:i]];
    [[self lastItem] setRepresentedObject:[objects objectAtIndex:i]];
  }
}

-(BOOL)selectItemWithRepresentedObject_:(id)obj {
  int len = [self numberOfItems];
  int i;
  for(i=0; i<len; i++) {
    if ([obj isEqual:[[self itemAtIndex:i] representedObject]]) {
      [self selectItemAtIndex:i];
      return YES;
    }
  }
  return NO;
}

@end

@implementation NSTextView (DCAppKitAdditions)
-(void)appendText:(NSString *)text scrollToEnd_:(BOOL)scroll {
  [self replaceCharactersInRange:NSMakeRange([[self string] length],0)
                      withString:text];
  if (scroll) [self scrollRangeToVisible:NSMakeRange([[self string] length], 0)];
}
@end

@implementation NSWindow (DCAppKitAdditions)
-(float)userSpaceScaleFactor_ {
  // Linux compiler doesn't like cast to float
#ifndef GNUSTEP
  if ([self respondsToSelector:@selector(userSpaceScaleFactor)]) {
    return (float)[self userSpaceScaleFactor];
  }
#endif
  return 1.0f;
}
@end


@implementation NSObject (DCMiscAdditions)
-(NSArray *)arrayWithSelf_ {
  return [NSArray arrayWithObject:self];
}
@end

