//
//  SabotageGame.h
//  Gridlock
//
//  Created by Brian on 3/27/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "Game.h"

@interface SabotageGame : Game {

}

-(DCHypergridPosition *)goalPositionForPlayer:(int)pnum;
-(int)playerWithGoalPosition:(DCHypergridPosition *)pos;

@end
