/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "GravityGame.h"

static int WIN_LENGTH = 4;

@implementation GravityGame

-(int)winningLength {
  return WIN_LENGTH;
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *moves = [NSMutableArray array];
  NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:0];
  id pos;
  while (pos=[pe nextObject]) {
    BOOL good = NO;
    int r = [pos row];
    int c = [pos column];
    int boardSize = [self numberOfRows]; // assuming square board
    // edges are always legal
    if (r==0 || c==0 || r==boardSize-1 || c==boardSize-1) good = YES;
    else {
      // find the edge or corner closest to the position
      int dr=0, dc=0;
      int mid = boardSize/2;
      int rdiff = (r<mid) ? r : boardSize-1-r;
      int cdiff = (c<mid) ? c : boardSize-1-c;
      // both these conditions will be true for positions on diagonal
      if (rdiff<=cdiff) {
        dr = (r<mid) ? -1 : +1; // pulled toward bottom or top row
      }
      if (cdiff<=rdiff) {
        dc = (c<mid) ? -1 : +1; // pulled toward left or right column
      }
      good = ([self valueAtRow:r+dr column:c+dc]!=0);
    }
    if (good) [moves addObject:[pos arrayWithSelf_]];
  }
  return moves;
}

@end
