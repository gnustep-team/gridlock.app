//
//  DaggerAI.h
//  Gridlock
//
//  Created by Brian on Sun Feb 29 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GenericAI.h"

@interface DaggerAI : GenericAI {
  int p1DaggerWeight;
  int p2DaggerWeight;
  int crownPositionWeight;
}

@end
