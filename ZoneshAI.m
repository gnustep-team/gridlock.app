/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "ZoneshAI.h"
#import "ZoneshGame.h"

static inline int min(int a, int b) {
  return (a<b) ? a : b;
}
static inline int max(int a, int b) {
  return (a>b) ? a : b;
}

@implementation ZoneshAI

-(int)piecesSupportingPostion:(id)pos forPlayer:(int)pnum inGame:(ZoneshGame *)game {
  int count = 0;
  int maxr = [game numberOfRows];
  int maxc = [game numberOfColumns];
  int dr,dc;
  for(dr=-1; dr<=1; dr++) {
    int r = [pos row]+dr;
    for(dc=-1; dc<=1; dc++) {
      int c = [pos column]+dc;
      if (r>=0 && r<maxr && c>=0 && c<maxc && (r!=0 || c!=0)) {
        int owner = [game valueAtRow:r column:c];
        if (owner==pnum) {
          // we have support if the neighbor isn't diagonal *or* isn't in the home zone
          if (dr==0 || dc==0 || pnum!=[game playerWithHomeZoneContainingPosition:[DCHypergridPosition positionWithRow:r column:c]]) {
            ++count;
          }
        }
      }
    }
  }
  return count;
}


-(int)distanceFromOriginForPosition:(id)pos andPlayer:(int)pnum inGame:(Game *)game {
  int dist = (pnum==1) ? min([pos row],[pos column]) :
                         [game numberOfRows]-1-max([pos row],[pos column]);
  return dist;
}

-(BOOL)position:(id)pos isControlledByPlayer:(int)pnum inGame:(ZoneshGame *)game {
  int cpnum = [game currentPlayerNumber];
  int mysupport = [self piecesSupportingPostion:pos forPlayer:pnum inGame:game];
  int oppsupport = [self piecesSupportingPostion:pos forPlayer:[game playerNumberMovingAfterPlayer:pnum] inGame:game];
  BOOL hasControl = (mysupport>oppsupport || (mysupport==oppsupport && cpnum==pnum));
  return hasControl;
}

-(int)threatWeightForGame:(ZoneshGame *)game player:(int)pnum {
  int utility = 0;
  NSEnumerator *pe = [[game grid] enumeratorForPositionsWithValue:pnum];
  id pos;

  int maxdc = 0;
  while (pos=[pe nextObject]) {
    int dist = [self distanceFromOriginForPosition:pos andPlayer:pnum inGame:game];
    BOOL control = [self position:pos isControlledByPlayer:pnum inGame:game];
    if (control) {
      utility += 2*dist+2;
      if (dist>maxdc) maxdc=dist;
    }
    else {
      utility += dist+1;
    }
  }
  utility += maxdc;
  return utility;
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self threatWeightForGame:game player:pnum] -
  [self threatWeightForGame:game player:[game nextPlayerNumber]];
}

@end
