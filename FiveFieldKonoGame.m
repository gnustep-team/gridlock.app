/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "FiveFieldKonoGame.h"


@implementation FiveFieldKonoGame

-(void)reset {
  [super reset];
  [self setGrid:[DCHypergrid gridWithRows:5 columns:5]];
  [self fillFirstRows:1];
  [self setValue:1 atRow:1 column:0];
  [self setValue:1 atRow:1 column:4];
  [self setValue:2 atRow:3 column:0];
  [self setValue:2 atRow:3 column:4];
  [self setCurrentPlayerNumber:1];
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *moves = [NSMutableArray array];
  int pnum = [self currentPlayerNumber];
  int maxr = [self numberOfRows];
  int maxc = [self numberOfColumns];
  NSEnumerator *pe = [[self grid] positionEnumerator];
  id pos;
  while (pos=[pe nextObject]) {
    if ([self valueAtPosition:pos]==pnum) {
      int r = [pos row];
      int c = [pos column];
      // check diagonals
      if (r+1<maxr && c+1<maxc && [self valueAtRow:r+1 column:c+1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos,[DCHypergridPosition positionWithRow:r+1 column:c+1],nil]];
      }
      if (r+1<maxr && c>0 && [self valueAtRow:r+1 column:c-1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos,[DCHypergridPosition positionWithRow:r+1 column:c-1],nil]];
      }
      if (r>0 && c+1<maxc && [self valueAtRow:r-1 column:c+1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos,[DCHypergridPosition positionWithRow:r-1 column:c+1],nil]];
      }
      if (r>0 && c>0 && [self valueAtRow:r-1 column:c-1]==0) {
        [moves addObject:[NSArray arrayWithObjects:pos,[DCHypergridPosition positionWithRow:r-1 column:c-1],nil]];
      }
    }
  }
  if ([moves count]==0) [moves addObject:[NSArray array]];
  return moves;
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  [self resetFutureGrid];
  if ([positions count]==2) {
    [[self futureGrid] setValue:0 atPosition:[positions objectAtIndex:0]];
    [[self futureGrid] setValue:[self currentPlayerNumber] atPosition:[positions objectAtIndex:1]];
  }
  return YES;
}

-(int)hasPlayerWon:(int)pnum {
  int cols = [self numberOfColumns];
  // check top rows for player 1, bottom for player 2
  int lastr = (pnum==1) ? [self numberOfRows]-1 : 0;
  int dr = (pnum==1) ? -1 : 1;
  int c;
  // must occupy all positions in last row
  for(c=0; c<cols; c++) {
    if (pnum!=[self valueAtRow:lastr column:c]) return NO;
  }
  // must also occupy first and last columns in second row
  if (pnum!=[self valueAtRow:lastr+dr column:0]) return NO;
  if (pnum!=[self valueAtRow:lastr+dr column:cols-1]) return NO;
  return YES;
}

-(int)winningPlayer {
  if ([self hasPlayerWon:1]) return 1;
  if ([self hasPlayerWon:2]) return 2;
  return 0;
}

-(BOOL)isGameOver {
  return ([self hasPlayerWon:1] || [self hasPlayerWon:2]);
}

@end
