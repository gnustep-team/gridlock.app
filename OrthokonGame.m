/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "OrthokonGame.h"
#import "AnyDirectionUntilBlockedMoveRule.h"

@implementation OrthokonGame

-(void)reset {
  [super reset];
  [self setGrid:[DCHypergrid gridWithRows:[[[self configurationInfo] objectForKey:@"rows"] intValue]
                                  columns:[[[self configurationInfo] objectForKey:@"cols"] intValue]]];
  [self fillFirstRows:1];
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  static int ROW_DIRECTIONS[] = {+1, 0, 0,-1};
  static int COL_DIRECTIONS[] = { 0,-1,+1, 0};
  int pnum = [self currentPlayerNumber];
  int oppnum = [self nextPlayerNumber];
  id endpos = [positions objectAtIndex:1];
  int r = [endpos row];
  int c = [endpos column];
  int i;
  
  [self resetFutureGrid];
  [[self futureGrid] setValue:0 atPosition:[positions objectAtIndex:0]];
  [[self futureGrid] setValue:pnum atPosition:endpos];
  // flip opposing pieces up, down, left, right of destination
  for(i=0; i<4; i++) {
    if ([self isValidRow:r+ROW_DIRECTIONS[i] column:c+COL_DIRECTIONS[i]] &&
        [self valueAtRow:r+ROW_DIRECTIONS[i] column:c+COL_DIRECTIONS[i]]==oppnum) {
      [[self futureGrid] setValue:pnum atRow:r+ROW_DIRECTIONS[i] column:c+COL_DIRECTIONS[i]];
    }
  }
  return YES;
}

-(int)winningPlayer {
  // if the game is over, the current player has lost (can't move)
  return [self nextPlayerNumber];
}

-(BOOL)isGameOver {
  return ![AnyDirectionUntilBlockedMoveRule gameHasAnyMoves:self];
}

-(NSArray *)allValidMoveSequences {
  return [AnyDirectionUntilBlockedMoveRule allValidMoveSequences:self];
}

@end
