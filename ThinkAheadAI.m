//
//  ThinkAheadAI.m
//  Gridlock
//
//  Created by Brian on 3/26/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "ThinkAheadAI.h"


@implementation ThinkAheadAI

-(int)searchDepthForGame:(Game *)game {
  int numUsed = [[game grid] numberOfCellsWithValue:-11];
  int index=numUsed/16;
  int d = [[depths objectAtIndex:index] intValue];
  //NSLog(@"Using search depth:%d", d);
  return d;
}

@end
