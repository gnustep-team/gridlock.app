/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "Preferences.h"
#import "CocoaAdditions.h"

static NSDictionary* defaultPreferences() {
  NSMutableDictionary *dict = [NSMutableDictionary dictionary];
  [dict setObject:[[NSColor redColor] archivedData_] forKey:@"player1PieceColor"];
  [dict setObject:[[NSColor blueColor] archivedData_] forKey:@"player2PieceColor"];
  [dict setObject:[[NSColor redColor] archivedData_] forKey:@"player1HighlightColor"];
  [dict setObject:[[NSColor blueColor] archivedData_] forKey:@"player2HighlightColor"];
  [dict setObject:@"red.tiff"  forKey:@"player1ImageName"];
  [dict setObject:@"blue.tiff" forKey:@"player2ImageName"];
  // these values are strings because GNUstep doesn't seem to work with NSNumbers
  [dict setObject:@"1" forKey:@"animateCapturedPieces"];
  [dict setObject:@"1" forKey:@"useBitmapPieces"];
  return dict;
}

static Preferences *_instance;

@implementation Preferences

-(id)init {
  self = [super init];
  [[NSUserDefaults standardUserDefaults] registerDefaults:defaultPreferences()];
  return self;
}

+(Preferences *)sharedInstance {
  if (!_instance) {
    _instance = [[self alloc] init];
  }
  return _instance;
}

-(NSColor *)player1PieceColor {
  return [[NSUserDefaults standardUserDefaults] archivedObjectForKey:@"player1PieceColor"];
}
-(void)setPlayer1PieceColor:(NSColor *)value {
  [[NSUserDefaults standardUserDefaults] setArchivedObject:value forKey:@"player1PieceColor"];
}
-(NSColor *)player2PieceColor {
  return [[NSUserDefaults standardUserDefaults] archivedObjectForKey:@"player2PieceColor"];
}
-(void)setPlayer2PieceColor:(NSColor *)value {
  [[NSUserDefaults standardUserDefaults] setArchivedObject:value forKey:@"player2PieceColor"];
}

-(NSColor *)pieceColorForPlayerNumber:(int)pnum {
  return [self valueForKey:[NSString stringWithFormat:@"player%dPieceColor",pnum]];
}

-(NSColor *)player1HighlightColor {
  return [[NSUserDefaults standardUserDefaults] archivedObjectForKey:@"player1HighlightColor"];
}
-(void)setPlayer1HighlightColor:(NSColor *)value {
  [[NSUserDefaults standardUserDefaults] setArchivedObject:value forKey:@"player1HighlightColor"];
}
-(NSColor *)player2HighlightColor {
  return [[NSUserDefaults standardUserDefaults] archivedObjectForKey:@"player2HighlightColor"];
}
-(void)setPlayer2HighlightColor:(NSColor *)value {
  [[NSUserDefaults standardUserDefaults] setArchivedObject:value forKey:@"player2HighlightColor"];
  
}

-(NSColor *)highlightColorForPlayerNumber:(int)pnum {
  return [self valueForKey:[NSString stringWithFormat:@"player%dHighlightColor",pnum]];
}


-(NSString *)player1ImageName {
  return [[NSUserDefaults standardUserDefaults] objectForKey:@"player1ImageName"];
}
-(void)setPlayer1ImageName:(NSString *)value {
  [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"player1ImageName"];
}
-(NSString *)player2ImageName {
  return [[NSUserDefaults standardUserDefaults] objectForKey:@"player2ImageName"];
}
-(void)setPlayer2ImageName:(NSString *)value {
  [[NSUserDefaults standardUserDefaults] setObject:value forKey:@"player2ImageName"];
}

-(NSString *)imageNameForPlayerNumber:(int)pnum {
  if (pnum>0) return [self valueForKey:[NSString stringWithFormat:@"player%dImageName",pnum]];
  else return @"None";
}

-(BOOL)useBitmapPieces {
  return [[NSUserDefaults standardUserDefaults] integerForKey:@"useBitmapPieces"];
}
-(void)setUseBitmapPieces:(BOOL)value {
  [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"useBitmapPieces"];
}

-(BOOL)animateCapturedPieces {
  return [[NSUserDefaults standardUserDefaults] integerForKey:@"animateCapturedPieces"];
}
-(void)setAnimateCapturedPieces:(BOOL)value {
  [[NSUserDefaults standardUserDefaults] setInteger:value forKey:@"animateCapturedPieces"];
}

-(NSString *)initialGame {
  return [[NSUserDefaults standardUserDefaults] objectForKey:@"initialGame"];
}
-(void)setInitialGame:(NSString *)game {
  [[NSUserDefaults standardUserDefaults] setObject:game forKey:@"initialGame"];
}

-(NSString *)networkUsername {
  NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"networkUsername"];
  if (!name) name = NSUserName();
  return name;
}
-(void)setNetworkUsername:(NSString *)name {
  [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"networkUsername"];
}


-(NSString *)configurationForGame:(NSString *)game {
  NSString *key = [NSString stringWithFormat:@"config.%@",game];
  return [[NSUserDefaults standardUserDefaults] objectForKey:key];  
}
-(void)setConfiguration:(NSString *)cfg forGame:(NSString *)game {
  NSString *key = [NSString stringWithFormat:@"config.%@",game];
  if (cfg) [[NSUserDefaults standardUserDefaults] setObject:cfg forKey:key];
  else [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}

// nil->unassigned, @""->human
-(NSString *)aiTypeForGame:(NSString *)game player:(int)player {
  NSString *key = [NSString stringWithFormat:@"ai.%@.%d",game,player];
  return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)setAIType:(NSString *)aiType forGame:(NSString *)game player:(int)player {
  NSString *key = [NSString stringWithFormat:@"ai.%@.%d",game,player];
  if (aiType==nil) [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
  else [[NSUserDefaults standardUserDefaults] setObject:aiType forKey:key];
}

@end
