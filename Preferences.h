/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <AppKit/NSColor.h>

@interface Preferences : NSObject {

}

+(Preferences *)sharedInstance;

-(NSColor *)player1PieceColor;
-(void)setPlayer1PieceColor:(NSColor *)value;
-(NSColor *)player2PieceColor;
-(void)setPlayer2PieceColor:(NSColor *)value;

-(NSColor *)pieceColorForPlayerNumber:(int)pnum;

-(NSColor *)player1HighlightColor;
-(void)setPlayer1HighlightColor:(NSColor *)value;
-(NSColor *)player2HighlightColor;
-(void)setPlayer2HighlightColor:(NSColor *)value;

-(NSColor *)highlightColorForPlayerNumber:(int)pnum;

-(NSColor *)pieceColorForPlayerNumber:(int)pnum;

-(NSString *)player1ImageName;
-(void)setPlayer1ImageName:(NSString *)value;
-(NSString *)player2ImageName;
-(void)setPlayer2ImageName:(NSString *)value;

-(NSString *)imageNameForPlayerNumber:(int)pnum;

-(BOOL)useBitmapPieces;
-(void)setUseBitmapPieces:(BOOL)value;

-(BOOL)animateCapturedPieces;
-(void)setAnimateCapturedPieces:(BOOL)value;

-(NSString *)initialGame;
-(void)setInitialGame:(NSString *)game;

-(NSString *)configurationForGame:(NSString *)game;
-(void)setConfiguration:(NSString *)cfg forGame:(NSString *)game;

-(NSString *)aiTypeForGame:(NSString *)game player:(int)player;
-(void)setAIType:(NSString *)aiType forGame:(NSString *)game player:(int)player;

-(NSString *)networkUsername;
-(void)setNetworkUsername:(NSString *)name;
  
@end
