/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import "hypergrid.h"
#import "DCHypergridPosition.h"

@interface DCHypergrid : NSObject {
  hypergrid *_cgrid;
}

-(id)initWithNumberOfDimensions:(int)numdim sizes:(int *)sizes;

-(id)initWithCHypergrid:(hypergrid *)gridptr;

-(id)copy;
-(void)copyValuesToGrid:(DCHypergrid *)gridCopy;

-(int)numberOfDimensions;
-(int)numberOfCells;

-(gridvalue_t)valueAtPosition:(DCHypergridPosition *)coords;
-(gridvalue_t)valueAtCoordinateArray:(int *)array;

-(BOOL)isValidPosition:(DCHypergridPosition *)position;
-(BOOL)isValidCoordinateArray:(int *)array;
-(BOOL)isValidRow:(int)r column:(int)c;

-(id)setValue:(gridvalue_t)value atPosition:(DCHypergridPosition *)coords;
-(id)setValue:(gridvalue_t)value atCoordinateArray:(int *)array;

-(void)setValue:(gridvalue_t)value atPositions:(NSArray *)positions;

-(int)sizeOfDimension:(int)dim;

-(int)nonzeroNeighborsOfPosition:(DCHypergridPosition *)coords;
-(int)nonzeroNeighborsOfPosition:(DCHypergridPosition *)coords wrapping:(BOOL)wrap;

-(int)numberOfNeighborsOfPosition:(DCHypergridPosition *)pos withValue:(int)value;

-(int)numberOfCellsWithValue:(int)value;
-(BOOL)hasCellWithValue:(int)value;

-(BOOL)isEqualToHypergrid:(DCHypergrid *)cmp;

-(NSArray *)neighborsOfPosition:(DCHypergridPosition *)coords distance:(int)dist;

-(NSEnumerator *)positionEnumerator;
-(NSEnumerator *)enumeratorForPositionsWithValue:(int)value;

-(DCHypergridPosition *)positionWithValue:(int)value;
-(NSArray *)allPositionsWithValue:(int)value;

-(NSArray *)connectedGroupsWithValue:(int)value onlyOrthoganal:(BOOL)ortho;

-(hypergrid *)cHypergrid;

-(id)propertyList;
-(id)initFromPropertyList:(id)plist;

// convenience methods for 2d grids...put these in a separate category?
-(id)initWithRows:(int)r columns:(int)c;
+(id)gridWithRows:(int)r columns:(int)c;
-(int)valueAtRow:(int)r column:(int)c;
-(void)setValue:(int)value atRow:(int)r column:(int)c;
-(int)numberOfRows;
-(int)numberOfColumns;

-(void)dealloc;

@end
