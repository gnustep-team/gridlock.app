//
//  FusionGame.m
//  Gridlock
//
//  Created by Brian on 11/24/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "FusionGame.h"


@implementation FusionGame

-(void)reset {
  [super reset];
  [self createGridFromConfiguration];
  
  [self fillRandomEmptyCellsWithValue:-1 count:[[[self configurationInfo] objectForKey:@"protons"] intValue]];
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *moves = [NSMutableArray array];
  NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:-1];
  id pos;
  while (pos=[pe nextObject]) {
    int r0 = [pos row];
    int c0 = [pos column];
    int dr, dc;
    for(dr=-1; dr<=+1; dr++) {
      for(dc=-1; dc<=+1; dc++) {
        if (dc==0 && dr==0) continue;
        // try moves along this direction until we hit an edge or another piece
        BOOL done = NO;
        int dist = 0;
        while (!done) {
          dist++;
          int r1 = r0+dist*dr;
          int c1 = c0+dist*dc;
          if (![self isValidRow:r1 column:c1] || 0!=[self valueAtRow:r1 column:c1]) {
            done = YES;
          }
          else {
            // valid move if there's an "atom" adjacent to the destination
            BOOL valid = NO;
            int dr2, dc2;
            for(dr2=-1; dr2<=+1 && !valid; dr2++) {
              for(dc2=-1; dc2<=+1 && !valid; dc2++) {
                if ([self isValidRow:r1+dr2 column:c1+dc2] && [self valueAtRow:r1+dr2 column:c1+dc2]>0) {
                  valid = YES;
                }
              }
            }
            if (valid) [moves addObject:[NSArray arrayWithObjects:pos, [DCHypergridPosition positionWithRow:r1 column:c1], nil]];
          }
        }
      }
    }
  }
  return moves;
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  int pnum = [self currentPlayerNumber];
  id pos0 = [positions objectAtIndex:0];
  id pos1 = [positions objectAtIndex:1];
  NSArray *neighbors = [[self grid] neighborsOfPosition:pos1 distance:1];
  NSEnumerator *ne = [neighbors objectEnumerator];
  id npos;
  
  [self resetFutureGrid];
  
  // convert all neighbors to current player
  while (npos=[ne nextObject]) {
    if ([self valueAtPosition:npos]!=0) {
      [[self futureGrid] setValue:pnum atPosition:npos];
    }
  }
  // force start and destination to correct values
  [[self futureGrid] setValue:0 atPosition:pos0];
  [[self futureGrid] setValue:pnum atPosition:pos1];
  return YES;
}


-(BOOL)isGameOver {
  return ![[self grid] hasCellWithValue:-1];
}

-(BOOL)showScores {
  return YES;
}

@end
