//
//  CatsAndDogsViewDelegate.m
//  Gridlock
//
//  Created by Brian on Mon Aug 18 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import "CatsAndDogsViewDelegate.h"
#import <AppKit/AppKit.h>
#import "CatsAndDogsGame.h"
#import "Preferences.h"

@implementation CatsAndDogsViewDelegate

-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(id)game {
  // +X: player X has placed a piece at this position
  // -X: player X has a piece bordering this position, opponent does not
  // -3: both players have pieces bordering this position
  NSColor *tintColor = nil;
  if (abs(value)>2) {
    tintColor = [NSColor grayColor];
  }
  else if (abs(value)==1) {
    tintColor = [[[Preferences sharedInstance] highlightColorForPlayerNumber:1] blendedColorWithFraction:0.75 ofColor:[NSColor whiteColor]];
  }
  else if (abs(value)==2) {
    tintColor = [[[Preferences sharedInstance] highlightColorForPlayerNumber:2] blendedColorWithFraction:0.75 ofColor:[NSColor whiteColor]];
  }
  if (tintColor) {
    [tintColor set];
    NSRectFill(rect);
  }
  return NO;
}

-(BOOL)disableAnimation {
  return YES;
}

@end
