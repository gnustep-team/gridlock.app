/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "PrefsWindowController.h"
#import "Preferences.h"


@implementation PrefsWindowController

-(void)buildImagePopups {
  NSArray *popups = [NSArray arrayWithObjects:player1ImagePopup,player2ImagePopup,nil];
  NSEnumerator *pe = [popups objectEnumerator];
  id popup;
  while (popup=[pe nextObject]) {
    NSEnumerator *de = [imageDefinitions objectEnumerator];
    id imageDef;
    [popup removeAllItems];
    while (imageDef=[de nextObject]) {
      [popup addItemWithTitle:[imageDef objectForKey:@"name"]];
      [[popup lastItem] setRepresentedObject:imageDef];
    }
  }
}

-(void)awakeFromNib {
  radioGroup = [[NSArray alloc] initWithObjects:imageRadioButton,colorRadioButton,nil];
  imageDefinitions = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"PieceImages" ofType:@"plist"]];
  [self buildImagePopups];
}

-(void)dealloc {
  [radioGroup release];
  [imageDefinitions release];
  [super dealloc];
}

-(void)showWindow {
  [window makeKeyAndOrderFront:self];
  [self updateFromPreferences];
}

- (IBAction)cancel:(id)sender {
  [window orderOut:self];
}

-(NSColor *)highlightColorForDictionary:(NSDictionary *)dict {
  id value = [dict objectForKey:@"highlight"];
  if ([value isKindOfClass:[NSString class]]) {
    return [[NSColor class] performSelector:NSSelectorFromString(value)];
  }
  else if ([value isKindOfClass:[NSDictionary class]]) {
    float red   = [[value objectForKey:@"red"] floatValue];
    float green = [[value objectForKey:@"green"] floatValue];
    float blue  = [[value objectForKey:@"blue"] floatValue];
    return [NSColor colorWithCalibratedRed:red green:green blue:blue alpha:1.0];
  }
  return nil;
}

- (IBAction)applyPreferences:(id)sender {
  [[Preferences sharedInstance] setPlayer1PieceColor:[player1ColorWell color]];
  [[Preferences sharedInstance] setPlayer2PieceColor:[player2ColorWell color]];

  [[Preferences sharedInstance] setPlayer1ImageName:[[[player1ImagePopup selectedItem] representedObject] objectForKey:@"file"]];
  [[Preferences sharedInstance] setPlayer2ImageName:[[[player2ImagePopup selectedItem] representedObject] objectForKey:@"file"]];
  
  [[Preferences sharedInstance] setAnimateCapturedPieces:([animateCheckbox state]==NSOnState)];
  [[Preferences sharedInstance] setUseBitmapPieces:([imageRadioButton state]==NSOnState)];

  // use either selected colors or colors defined in plist for highlight colors
  if ([[Preferences sharedInstance] useBitmapPieces]) {
    [[Preferences sharedInstance] setPlayer1HighlightColor:[self highlightColorForDictionary:[[player1ImagePopup selectedItem] representedObject]]];
    [[Preferences sharedInstance] setPlayer2HighlightColor:[self highlightColorForDictionary:[[player2ImagePopup selectedItem] representedObject]]];
  }
  else {
  [[Preferences sharedInstance] setPlayer1HighlightColor:[player1ColorWell color]];
  [[Preferences sharedInstance] setPlayer2HighlightColor:[player2ColorWell color]];
  }
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayerColorsChangedNotification"
                                                      object:nil];
}

-(IBAction)savePreferences:(id)sender {
  [self applyPreferences:sender];
  [window orderOut:self];
}

-(NSString *)imageTitleForFilename:(NSString *)file {
  NSEnumerator *de = [imageDefinitions objectEnumerator];
  id def;
  while (def=[de nextObject]) {
    if ([file isEqualToString:[def objectForKey:@"file"]]) {
      return [def objectForKey:@"name"];
    }
  }
  return nil;
}

-(void)updateFromPreferences {
  [player1ColorWell setColor:[[Preferences sharedInstance] player1PieceColor]];
  [player2ColorWell setColor:[[Preferences sharedInstance] player2PieceColor]];

  [player2ImagePopup selectItemWithTitle:[self imageTitleForFilename:[[Preferences sharedInstance] player2ImageName]]];
  [player1ImagePopup selectItemWithTitle:[self imageTitleForFilename:[[Preferences sharedInstance] player1ImageName]]];
  
  [animateCheckbox setState:([[Preferences sharedInstance] animateCapturedPieces] ? NSOnState: NSOffState)];
  
  [imageRadioButton setState:([[Preferences sharedInstance] useBitmapPieces] ? NSOnState : NSOffState)];
  [colorRadioButton setState:([[Preferences sharedInstance] useBitmapPieces] ? NSOffState : NSOnState)];
}

-(IBAction)radioButtonSelected:(id)sender {
  NSEnumerator *re = [radioGroup objectEnumerator];
  id button;
  while (button=[re nextObject]) {
    [button setState:((button==sender) ? NSOnState : NSOffState)];
  }
}

@end
