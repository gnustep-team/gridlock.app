/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "AppController.h"
#import "DCHypergrid.h"
#import "GameController.h"
#import "PrefsWindowController.h"
#import "ClientController.h"
#import "ServerController.h"
/*
static void test2() {
  DCHypergrid *grid = [[[DCHypergrid alloc] initWithRows:8 columns:8] autorelease];
  DCHypergridPosition *pos1 = [DCHypergridPosition positionWithRow:2 column:3];
  DCHypergridPosition *pos2 = [DCHypergridPosition positionWithRow:4 column:5];
  DCHypergridPosition *pos3 = [DCHypergridPosition positionWithRow:2 column:3];
  DCHypergridPosition *pos4 = [DCHypergridPosition positionWithRow:4 column:5];

  NSArray *array1 = [NSArray arrayWithObjects:pos1,pos2,nil];
  NSArray *array2 = [NSArray arrayWithObjects:pos3,pos4,nil];
  NSArray *array3 = [NSArray arrayWithObjects:pos1,pos3,pos3,nil];
  NSLog(@"%d %d %d %d %d %d", [pos1 hash], [pos2 hash], [pos3 hash], [pos4 hash], [array1 hash], [array3 hash]);
  NSLog(@"%d %d %d", [pos1 isEqual:pos3], [array1 isEqual:array2], [array1 isEqual:array3]);

  {
    NSData *gdata = [NSArchiver archivedDataWithRootObject:grid];
    DCHypergrid *gridcopy = [NSUnarchiver unarchiveObjectWithData:gdata];
    NSLog(@"%d %d", [grid numberOfDimensions], [gridcopy numberOfDimensions]);
  }
}
*/
@implementation AppController

-(void)applicationDidFinishLaunching:(NSNotification *)note {
}

idAccessor(gameController, setGameController)

-(void)awakeFromNib {
  [self setGameController:[[[GameController alloc] init] autorelease]];
  [NSBundle loadNibNamed:@"GameWindow" owner:[self gameController]];
}

-(void)showPreferences:(id)sender {
  if (!prefsController) {
    prefsController = [[PrefsWindowController alloc] init];
    [NSBundle loadNibNamed:@"PrefsWindow" owner:prefsController];
  }
  [prefsController showWindow];
}

-(void)joinGame:(id)sender {
  if (!clientController) {
    clientController = [[ClientController alloc] init];
    [NSBundle loadNibNamed:@"ClientWindow" owner:clientController];
  }
  [clientController showWindow];

}

-(void)hostGame:(id)sender {
  if (!serverController) {
    serverController = [[ServerController alloc] init];
    [NSBundle loadNibNamed:@"ServerWindow" owner:serverController];
  }
  [serverController showWindow];

}

// testing methods
/*
-(void)runTest {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  int i;
  for(i=0; i<10000; i++) {
    //[self testGrid];
    if (i%100==0) {
      [pool release];
      pool = [[NSAutoreleasePool alloc] init];
      NSLog(@"%d", i);
    }
  }
  [pool release]; 
}

-(void)testGrid {
  DCHypergrid *grid;
  int dim[] = {8,8,8};
  int coordints[] = {2,4,5};
  NSArray *neighbors;
  DCHypergridPosition *coords;
  
  grid = [[[DCHypergrid alloc] initWithNumberOfDimensions:2 sizes:dim] autorelease];
  coords = [[[DCHypergridPosition alloc] initWithSize:2 data:coordints] autorelease];
  neighbors = [grid neighborsOfPosition:coords distance:2];
  NSLog(@"neighbors:%@", neighbors);
  //[coords debug];
  //NSLog(@"%d", [neighbors count]);
  //for(i=0; i<[neighbors count]; i++) {
  //  [[neighbors objectAtIndex:i] debug];
  //}
}
*/
@end
