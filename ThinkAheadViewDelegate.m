//
//  ThinkAheadViewDelegate.m
//  Gridlock
//
//  Created by Brian on 3/26/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "ThinkAheadViewDelegate.h"
#import "ImageStore.h"

@implementation ThinkAheadViewDelegate

-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(id)game {
  if (value==-11) {
    NSRect changeRect = NSInsetRect(rect, rect.size.width/3, rect.size.height/3);
    NSImage *wallImage = [[ImageStore defaultStore] bitmapImageWithName:@"wall.tiff" size:changeRect.size];
    [wallImage compositeToPoint:changeRect.origin operation:NSCompositeSourceOver];
  }
  else {
    if (value==-10) {
      // bigger move indicator
      NSRect changeRect = NSInsetRect(rect, rect.size.width/3, rect.size.height/3);
      [[NSColor blackColor] set];
      [[NSBezierPath bezierPathWithOvalInRect:changeRect] fill];
    }
    else {
      NSString *str = [[NSNumber numberWithInt:value] stringValue];
      // get text size at 12 points
      NSMutableDictionary *attrs = [NSMutableDictionary dictionary];
      [attrs setObject:[NSFont labelFontOfSize:12] forKey:NSFontAttributeName];
      [attrs setObject:((value>=0) ? [NSColor greenColor] : [NSColor redColor]) forKey:NSForegroundColorAttributeName];
      NSSize size = [str sizeWithAttributes:attrs];
      
      // scale to take 80% of the height
      double ratio = 0.6*rect.size.height/size.height;
      [attrs setObject:[NSFont labelFontOfSize:12*ratio] forKey:NSFontAttributeName];
      
      // draw in center
      size = [str sizeWithAttributes:attrs];
      [str drawAtPoint:NSMakePoint(rect.origin.x+(rect.size.width-size.width)/2,
                                   rect.origin.y+(rect.size.height-size.height)/2) withAttributes:attrs];
    }
  }
  return YES;
}

@end
