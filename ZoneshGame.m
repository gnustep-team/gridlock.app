/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "ZoneshGame.h"

@implementation ZoneshGame

-(int)playerWithHomeZoneContainingPosition:(id)pos {
  int r = [pos row];
  int c = [pos column];
  int boardSize = [self numberOfRows]; // assumes square
  if (r+c<boardSize-2) return 1;
  if (r+c>boardSize) return 2;
  return 0;
}

-(void)reset {
  [super reset];
  [self setGrid:[DCHypergrid gridWithRows:[[[self configurationInfo] objectForKey:@"rows"] intValue]
                                  columns:[[[self configurationInfo] objectForKey:@"cols"] intValue]]];
  {
    NSEnumerator *pe = [grid positionEnumerator];
    id pos;
    while(pos=[pe nextObject]) {
      [self setValue:[self playerWithHomeZoneContainingPosition:pos] atPosition:pos];
    }
  }
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  [self resetFutureGrid];
  [[self futureGrid] setValue:0 atPosition:[positions objectAtIndex:0]];
  [[self futureGrid] setValue:[self currentPlayerNumber] atPosition:[positions objectAtIndex:1]];
  return YES;
}

-(int)winningPlayer {
  if (![grid hasCellWithValue:2]) return 1;
  if (![grid hasCellWithValue:1]) return 2;
  // player 1 wins by getting to (maxr, maxc), player 2 wins by getting to (0,0)
  if ([self valueAtRow:0 column:0]==2) return 2;
  if ([self valueAtRow:[self numberOfRows]-1 column:[self numberOfColumns]-1]==1) return 1;
  return 0;
}

-(BOOL)isGameOver {
  return ([self winningPlayer]!=0);
}

-(NSArray *)allValidMoveSequences {
  int pnum = [self currentPlayerNumber];
  NSMutableArray *moves = [NSMutableArray array];
  NSEnumerator *pe = [[self grid] enumeratorForPositionsWithValue:pnum];
  id pos;
  while (pos=[pe nextObject]) {
    int zoneowner = [self playerWithHomeZoneContainingPosition:pos];
    NSArray *neighbors = [[self grid] neighborsOfPosition:pos distance:1];
    NSEnumerator *ne = [neighbors objectEnumerator];
    id npos;
    while (npos=[ne nextObject]) {
      if ([self valueAtPosition:npos]!=pnum) {
        // if in home zone, must be a horizontal or vertical move
        if (zoneowner!=pnum || [npos row]==[pos row] || [npos column]==[pos column]) {
          [moves addObject:[NSArray arrayWithObjects:pos,npos,nil]];
        }
      }
    }
  }
  return moves;
}

@end
