/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "GameHistory.h"
#import "AccessorMacros.h"

@class BoardView;

@protocol BoardViewTarget <NSObject>

-(void)boardView:(BoardView *)view clickedAtPosition:(DCHypergridPosition *)pos;
-(void)boardView:(BoardView *)view finishedMoveAnimation:(id)callbackObject;
-(void)boardView:(BoardView *)view mouseMovedOverPosition:(DCHypergridPosition *)pos;

@end

@protocol BoardViewDelegate <NSObject>
-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect;
-(BOOL)disableAnimation;
-(BOOL)drawCellAnimatingFromValue:(int)val1 toValue:(int)val2 fraction:(float)frac inRect:(NSRect)rect;
-(BOOL)drawCellWithValue:(int)value atRow:(int)row column:(int)col inRect:(NSRect)rect forGame:(Game *)game;
@end


@interface BoardView : NSView
{
  GameHistory *gameHistory;
  int moveStartRow;
  int moveStartColumn;
  id <BoardViewTarget> eventTarget;
  
  BOOL inAnimation;
  int animationFrames;
  int animationFrameCounter;
  NSTimer *animationTimer;

  DCHypergridPosition *lastMouseOverPosition;
  NSArray *highlightedPositions;
  
  // to control flashing of winning pieces
  NSTimer *flashTimer;
  NSArray *flashingPositions;
  BOOL flashState;
  
  NSArray *immediateChangePositions;
  NSArray *animatingPositions;
  id animationCallbackObject;

  NSMutableDictionary *pieceImages;

  id delegate;
}

idAccessor_h(gameHistory, setGameHistory)
idAccessor_h(highlightedPositions, setHighlightedPositions)
idAccessor_h(lastMouseOverPosition, setLastMouseOverPosition)
idAccessor_h(animatingPositions, setAnimatingPositions)

-(Game *)game;

-(id <BoardViewTarget>)eventTarget;
-(void)setEventTarget:(id <BoardViewTarget>)obj;

-(id <BoardViewDelegate>)delegate;
-(void)setDelegate:(id)delegate;

-(float)cellWidth;
-(float)cellHeight;

-(void)beginMoveAnimationForPositions:(NSArray *)animPositions immediatelyChangingPositions:(NSArray *)immdtPositions callbackObject:(id)obj;

-(void)updateColors:(id)sender;

-(NSRect)rectForGridPosition:(DCHypergridPosition *)pos;
-(void)drawCellAtPosition:(DCHypergridPosition *)position;
-(void)finishMove;

-(NSArray *)flashingPositions;
-(void)setFlashingPositions:(NSArray *)positions;
-(BOOL)isPositionFlashing:(id)pos;

@end

