/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "PlayerTurnIndicatorView.h"
#import "ImageStore.h"

@implementation PlayerTurnIndicatorView

-(void)awakeFromNib {
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(updatePieces)
                                               name:@"PlayerColorsChangedNotification"
                                             object:nil];  
}

-(void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self];
  [super dealloc];
}

-(int)playerNumber {
  return playerNumber;
}
-(void)setPlayerNumber:(int)pnum {
  if (playerNumber!=pnum) {
    playerNumber = pnum;
    [self setNeedsDisplay:YES];
  }
}

-(BOOL)visible {
  return visible;
}
-(void)setVisible:(BOOL)value {
  if ((value && !visible) || (!value && visible)) {
    visible = value;
    [self setNeedsDisplay:YES];
  }
}

-(void)drawRect:(NSRect)theRect {
  NSRect rect = [self bounds];
  NSImage *image = nil;

  [[NSColor clearColor] set];
  [NSBezierPath fillRect:rect];

  if (![self visible]) {
    rect = NSInsetRect(rect, rect.size.width/5, rect.size.height/5);
  }
  image = [[ImageStore defaultStore] pieceImageForPlayer:[self playerNumber]
                                                withSize:rect.size];
  [image dissolveToPoint:rect.origin fraction:([self visible] ? 1.0 : 0.5)];
}

-(void)updatePieces {
  [self setNeedsDisplay:YES];
}

@end
