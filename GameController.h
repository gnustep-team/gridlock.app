/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
#import "GameHistory.h"
#import "BoardView.h"
#import "PlayerTurnIndicatorView.h"
#import "GridlockGameState.h"
#import "AccessorMacros.h"

@interface GameController : NSObject <BoardViewTarget>
{
  // controls in the main window
    IBOutlet id boardView;
    IBOutlet id player1ScoreField;
    IBOutlet id player2ScoreField;
    IBOutlet id window;
    IBOutlet id player1TypePopup;
    IBOutlet id player2TypePopup;
    IBOutlet id gamePopup;
    IBOutlet id configurationPopup;
    IBOutlet id player1LabelField;
    IBOutlet id player2LabelField;
    IBOutlet PlayerTurnIndicatorView *player1TurnView;
    IBOutlet PlayerTurnIndicatorView *player2TurnView;
    IBOutlet id statusField;
    IBOutlet id passButton;

    // Mac OS X will set the drawer ivar, GNUstep will set the window
    IBOutlet id statisticsDrawer;
    IBOutlet id statisticsWindow; 
    // controls in the statistics window or drawer
    IBOutlet id p1Wins;
    IBOutlet id p2Wins;
    IBOutlet id aiTimeField;
    IBOutlet id aiNumMovesEvaluatedField;
    IBOutlet id aiMovesPerSecondField;
    IBOutlet id utilityField;
    IBOutlet id moveRecordTable;

    // Mac OS X will set the drawer ivar, GNUstep will set the window
    IBOutlet id networkWindow;
    IBOutlet id networkDrawer;
    // controls in the chat window or drawer
    IBOutlet id chatMessageField;
    IBOutlet id chatTranscriptArea;
    IBOutlet id networkStatusField;
    IBOutlet id disconnectButton;
    

    NSArray *availableGames;
    GridlockGameState *gameState;
    NSMutableArray *movePositions;
    BOOL inAnimation;

    NSMutableArray *playerWins;

    float widthDelta;
    
    int aiMoveID;
}

idAccessor_h(gameState, setGameState)

-(Game *)game;

-(NSArray *)movePositions;
-(void)setMovePositions:(NSMutableArray *)pos;
-(void)appendMovePosition:(id)position;
-(void)removeLastPositionFromMove;
-(void)clearMovePositions;

-(void)updateScores;

-(void)boardView:(BoardView *)view clickedAtPosition:(DCHypergridPosition *)pos;
-(void)boardView:(BoardView *)view finishedMoveAnimation:(id)callbackObject;

-(IBAction)playerTypeUpdated:(id)sender;
-(IBAction)configurationSelectedFromPopup:(id)sender;
-(IBAction)restartGame:(id)sender;
-(IBAction)sendChatMessage:(id)sender;
-(IBAction)showNetworkDrawer:(id)sender;
-(IBAction)showStatisticsDrawer:(id)sender;
-(IBAction)toggleNetworkDrawer:(id)sender;
-(IBAction)toggleStatisticsDrawer:(id)sender;

-(void)restartGame;
-(void)restartGameWithConfiguration:(GameConfiguration *)config;

-(BOOL)isWaitingForHumanMove;

-(BOOL)isInNetworkGame;

// private methods
-(void)buildConfigurationPopup;
-(void)buildGamePopup;
-(void)buildPlayerPopups;
-(BOOL)shouldMakeAIMove;
-(void)makeAIMove;
-(void)cancelPendingAIMoves;
-(void)updateNetworkStatus;
-(void)gameStateChanged;
-(void)doGameOver;
-(void)sendMoveToNetworkPlayers:(NSArray *)move;
-(void)dropAllNetConnections;
-(void)sendRestartToNetworkPlayers;

@end
