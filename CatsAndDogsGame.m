/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "CatsAndDogsGame.h"

static int BORDER_DIRECTIONS[] = {1,0, -1,0, 0,1, 0,-1};

@implementation CatsAndDogsGame

-(void)reset {
  [super reset];
  [self setGrid:[DCHypergrid gridWithRows:[[[self configurationInfo] objectForKey:@"rows"] intValue]
                                  columns:[[[self configurationInfo] objectForKey:@"cols"] intValue]]];
}

-(BOOL)position:(id)pos bordersPositionWithValue:(int)value {
  // check up, down, left, right
  int r = [pos row];
  int c = [pos column];
  if ([self isValidRow:r+1 column:c] && value==[self valueAtRow:r+1 column:c]) return YES;
  if ([self isValidRow:r-1 column:c] && value==[self valueAtRow:r-1 column:c]) return YES;
  if ([self isValidRow:r column:c+1] && value==[self valueAtRow:r column:c+1]) return YES;
  if ([self isValidRow:r column:c-1] && value==[self valueAtRow:r column:c-1]) return YES;
  return NO;
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  int i;
  id movepos = [positions lastObject];
  int pnum = [self currentPlayerNumber];
  int oppnum = [self nextPlayerNumber];
  
  [self resetFutureGrid];
  [[self futureGrid] setValue:pnum atPosition:movepos];

  for(i=0; i<8; i+=2) {
    id pos = [DCHypergridPosition positionWithRow:[movepos row]+BORDER_DIRECTIONS[i]
                                           column:[movepos column]+BORDER_DIRECTIONS[i+1]];
    if ([self isPositionValid:pos]) {
      int value = [self valueAtPosition:pos];
      // value will change if position was uncontrolled or controlled only by enemy
      if (value==0) {
        // current player gains sole control
        [[self futureGrid] setValue:-pnum atPosition:pos];
      } 
      else if (value==-oppnum) {
        // both gain control
        [[self futureGrid] setValue:-3 atPosition:pos];
      }
    }
  }  
  return YES;
}

-(NSArray *)allValidMoveSequences {
  NSMutableArray *sequences = [NSMutableArray array];
  int pnum = [self currentPlayerNumber];
  NSEnumerator *pe = [[self grid] positionEnumerator];
  id pos;
  while (pos=[pe nextObject]) {
    int value = [self valueAtPosition:pos];
    if (value==0 || value==-pnum) {
      [sequences addObject:[pos arrayWithSelf_]];
    }
  }
  return sequences;
}

-(BOOL)isGameOver {
  return !([[self grid] hasCellWithValue:0] ||
           [[self grid] hasCellWithValue:0-[self currentPlayerNumber]]);
}

-(int)winningPlayer {
  return [self nextPlayerNumber];
}

@end
