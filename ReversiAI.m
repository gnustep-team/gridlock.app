/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "ReversiAI.h"
#import "ReversiGame.h"

@implementation ReversiAI

-(id)init {
  self = [super init];
  // assign default weights
  // Reversi strategy says not to grab edges early, but an edgeWeight of 0 loses to 2.
  edgeWeight = 2.0f;
  cornerWeight = 8.0f;
  return self;
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  // count cells by middle, edges, and corners
  int mymiddle=0, myedges=0, mycorners=0, oppmiddle=0, oppedges=0, oppcorners=0;
  int maxr = [game numberOfRows]-1;
  int maxc = [game numberOfColumns]-1;
  int oppnum = [game playerNumberMovingAfterPlayer:pnum];

  int mytotal  = [[game grid] numberOfCellsWithValue:pnum];
  int opptotal = [[game grid] numberOfCellsWithValue:oppnum];
  int filledcells = mytotal + opptotal;
  // value of -1 indicates unplayable "wall" cell, currently not used
  int totalcells = [[game grid] numberOfCells] - [[game grid] numberOfCellsWithValue:-1];

  int r,c;
  for(r=0; r<=maxr; r++) {
    for(c=0; c<=maxc; c++) {
      int isRowEdge = (r==0 || r==maxr);
      int isColEdge = (c==0 || c==maxc);
      if (isRowEdge || isColEdge) {
        // if owned by a player, increment appropriate counter
        int value = [game valueAtRow:r column:c];
        if (value==pnum) {
          if (isRowEdge && isColEdge) mycorners++;
          else myedges++;
        }
        else if (value>0) {
          if (isRowEdge && isColEdge) oppcorners++;
          else oppedges++;
        }
      }
    }
  }
  mymiddle = mytotal-myedges-mycorners;
  oppmiddle = opptotal-oppedges-oppcorners;

  {
    // factors decay by the ratio of how many cells in the board are filled
    float factor = 1.0 - ((float)filledcells)/totalcells;
    float scale = 100.0f;
    float middlew = scale;
    float edgew   = scale*(1 + factor*edgeWeight);
    float cornerw = scale*(1 + factor*cornerWeight);

    int mypoints = (int)(middlew*mymiddle + edgew*myedges + cornerw*mycorners);
    int opppoints = (int)(middlew*oppmiddle + edgew*oppedges + cornerw*oppcorners);
    return mypoints-opppoints;
  }
}

@end
