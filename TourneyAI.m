//
//  TourneyAI.m
//  Gridlock
//
//  Created by Brian on Fri Jan 02 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "TourneyAI.h"
#import "TourneyGame.h"

@implementation TourneyAI

-(id)init {
  self = [super init];
  regWeight = 4;
  kingMinWeight = 6;
  kingMaxWeight = 10;
  return self;
}

-(int)weightForKingAtPosition:(id)pos forPlayer:(int)pnum inGame:(Game *)game {
  int rowsFromBack = (pnum==1) ? [game numberOfRows]-[pos row]-1 : [pos row];
  double ratio = 1.0*rowsFromBack/([game numberOfRows]-2);
  return kingMinWeight + ratio*(kingMaxWeight-kingMinWeight);
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  TourneyGame *cgame = game;
  int myutility  = 0;
  int opputility = 0;
  int oppnum = [cgame playerNumberMovingAfterPlayer:pnum];
  int total = 0;
  NSEnumerator *ke;
  id pos;
  // regular pieces
  myutility  = regWeight*[cgame numberOfRegularPiecesForPlayer:pnum];
  opputility = regWeight*[cgame numberOfRegularPiecesForPlayer:oppnum];
  // kings are more valuable the closer they are to the first row
  ke = [[game grid] enumeratorForPositionsWithValue:-1];
  while (pos=[ke nextObject]) {
    myutility += [self weightForKingAtPosition:pos forPlayer:1 inGame:game];
  }
  ke = [[game grid] enumeratorForPositionsWithValue:-2];
  while (pos=[ke nextObject]) {
    opputility += [self weightForKingAtPosition:pos forPlayer:2 inGame:game];
  }
  
  total = (myutility-opputility)*1000;
  
  total += (int)(1000.0*[[self class] normalizedRatioOf:myutility to:opputility maxRatio:2.0]);
  return total;
}

@end
