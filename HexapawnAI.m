/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "HexapawnAI.h"
#import "HexapawnGame.h"

@implementation HexapawnAI

-(int)rowIncrementForPlayer:(int)pnum {
  return (pnum==1) ? 1 : -1;
}

-(int)piecesSupportingPostion:(id)pos forPlayer:(int)pnum inGame:(HexapawnGame *)game {
  BOOL canCaptureForward = [game forwardCapturesAllowed];
  BOOL canCaptureDiagonally = [game diagonalCapturesAllowed];
  int count=0;
  int dr = [self rowIncrementForPlayer:pnum];
  int backr = [pos row]-dr;
  int c = [pos column];
  if (backr>=0 && backr<[game numberOfRows]) {
    // straight behind
    if (canCaptureForward && [game valueAtRow:backr column:c]==pnum) count++;
    // behind left
    if (canCaptureDiagonally && c>0 && [game valueAtRow:backr column:c-1]==pnum) count++;
    // behind right
    if (canCaptureDiagonally && c<[game numberOfColumns]-1 && [game valueAtRow:backr column:c+1]==pnum) count++;
  }
  return count;
}


-(BOOL)position:(id)pos isControlledByPlayer:(int)pnum inGame:(HexapawnGame *)game {
  int cpnum = [game currentPlayerNumber];
  int mysupport = [self piecesSupportingPostion:pos forPlayer:pnum inGame:game];
  int oppsupport = [self piecesSupportingPostion:pos forPlayer:[game playerNumberMovingAfterPlayer:pnum] inGame:game];
  BOOL hasControl = (mysupport>oppsupport || (mysupport==oppsupport && cpnum==pnum));
  return hasControl;
}

-(int)controlFactorForPosition:(id)pos forPlayer:(int)pnum inGame:(HexapawnGame *)game {
  if (![self position:pos isControlledByPlayer:pnum inGame:game]) return 10;
  else {
    int c=[pos column];
    int dr = [self rowIncrementForPlayer:pnum];
    int maxr = [game numberOfRows]-1;
    int maxc = [game numberOfColumns]-1;
    int oppnum = [game nextPlayerNumber];
    // check for opposing pieces ahead in this column and to sides
    BOOL oppAhead = NO;
    BOOL oppToSide = NO;
    int r;
    for(r=[pos row]+dr; r>=0 && r<=maxr; r+=dr) {
      if (!oppAhead && [game valueAtRow:r column:c]==oppnum) oppAhead = YES;
      if (!oppToSide) {
        if (c>0 && [game valueAtRow:r column:c-1]) oppToSide = YES;
        else if (c<maxc && [game valueAtRow:r column:c+1]) oppToSide = YES;
      }
    }
    if (oppAhead && !oppToSide) return 10; // unlikely to get by
    if (oppAhead && oppToSide) return 15;
    if (!oppAhead && oppToSide) return 15;
    else return 20;
  }
}

-(int)threatWeightForGame:(HexapawnGame *)game player:(int)pnum {
  int utility = 0;
  NSEnumerator *pe = [[game grid] enumeratorForPositionsWithValue:pnum];
  id pos;
  while (pos=[pe nextObject]) {
    int dist = (pnum==1) ? [pos row]-1 : [game numberOfRows]-[pos row];
    int control = [self controlFactorForPosition:pos forPlayer:pnum inGame:game];
    utility += (dist+1)*control;
  }
  return utility;
}

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  return [self threatWeightForGame:game player:pnum] -
  [self threatWeightForGame:game player:[game nextPlayerNumber]];
}

-(int)searchDepthForGame:(Game *)game {
  // use higher search depth when pawns only move forward
  int index = [game diagonalMovesAllowed] ? 1 : 0;
  return [[searchDepths objectAtIndex:index] intValue];
}

@end

