//
//  FissionGame.h
//  Gridlock
//
//  Created by Brian on Sat Nov 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface FissionGame : Game {
}

-(BOOL)isSuicideVariation;

@end
