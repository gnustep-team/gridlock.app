/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "BombardmentAI.h"

@interface BombardmentAIGoodMoveEnumerator : NSEnumerator {
  Game *game;
  NSMutableArray *startPositions;
  int movesReturned;
  int movedr; // 1 or -1 depending on player number
  // these ivars store where we are in the process of searching for moves
  int startPositionIndex; // which position we're starting from
  int directionIndex;     // index of the neighbor (possible ending position) of the start position
  BOOL done;
}
@end

@implementation BombardmentAIGoodMoveEnumerator 

-(id)initWithGame:(Game *)g {
  if (self=[super init]) {
    game = [g retain];
    // get start positions, and randomize so that if the utilities are equal the same move won't always be made
    {
      NSEnumerator *pe = [[game grid] enumeratorForPositionsWithValue:[game currentPlayerNumber]];
      id pos;
      startPositions = [[NSMutableArray alloc] init];
      while (pos=[pe nextObject]) {
        [startPositions addObject:pos];
      }
      [startPositions randomizeObjects_];
      movedr = ([game currentPlayerNumber]==1) ? 1 : -1;
    }
  }
  return self;
}

-(void)dealloc {
  [game release];
  [startPositions release];
  [super dealloc];
}

-(id)nextObject {
  id move = nil;
  if (done) return nil;
  while (move==nil && !done) {
    // check current possible move
    id startpos = [startPositions objectAtIndex:startPositionIndex];
    int r = [startpos row];
    int c = [startpos column];
    if (directionIndex==0 && [game isValidRow:r+movedr column:c-1] && 
                             [game valueAtRow:r+movedr column:c-1]==0) {
      move = [NSArray arrayWithObjects:startpos, [DCHypergridPosition positionWithRow:r+movedr column:c-1], nil];
    }
    else if (directionIndex==1 && [game isValidRow:r+movedr column:c] && 
        [game valueAtRow:r+movedr column:c]==0) {
      move = [NSArray arrayWithObjects:startpos, [DCHypergridPosition positionWithRow:r+movedr column:c], nil];
    }
    else if (directionIndex==2 && [game isValidRow:r+movedr column:c+1] && 
             [game valueAtRow:r+movedr column:c+1]==0) {
      move = [NSArray arrayWithObjects:startpos, [DCHypergridPosition positionWithRow:r+movedr column:c+1], nil];
    }
    else if (directionIndex==3 && 
             [[game grid] numberOfNeighborsOfPosition:startpos withValue:[game nextPlayerNumber]]>0) {
      // only check detonation moves if 
      move = [NSArray arrayWithObjects:startpos, startpos, nil];
    }
    // increment counters and see if we're done
    ++directionIndex;
    if (directionIndex==4) {
      directionIndex = 0;
      if (++startPositionIndex==[startPositions count]) done=YES;
    }
  }
  return move;
}

@end



@implementation BombardmentAI

-(int)relativeUtilityForGame:(Game *)game player:(int)pnum {
  int utility = 0;
  NSEnumerator *pe = [[game grid] positionEnumerator];
  id pos;
  while (pos=[pe nextObject]) {
    int value = [game valueAtPosition:pos];
    if (value>0) {
      int row = (value==1) ? [pos row] : [game numberOfRows]-1-[pos row];
      int rweight = abs(row-1); // 2nd row is bad
      int friendlyNeighbors = [[game grid] numberOfNeighborsOfPosition:pos withValue:value];
      
      rweight += [[game grid] numberOfNeighborsOfPosition:pos withValue:[game playerNumberMovingAfterPlayer:value]] - friendlyNeighbors;
      if (row>2 && friendlyNeighbors==0) rweight += 3;
      
      utility += (10+rweight)*((value==pnum) ? 1 : -1);
    }
  }
  return utility;
}

-(NSEnumerator *)enumeratorForMovesToConsiderForGame:(Game *)game {
  return [[[BombardmentAIGoodMoveEnumerator alloc] initWithGame:game] autorelease];
}

@end
