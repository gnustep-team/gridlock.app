//
//  DiagonalsGame.h
//  Gridlock
//
//  Created by Brian on Sat Jul 10 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface DiagonalsGame : Game {
  id preparedMove;
  int p1Score;
  int p2Score;
}

@end
