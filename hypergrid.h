/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

typedef unsigned int gridvalue_t;

typedef struct hypergrid {
  int num_dimensions;
  int *dimension_sizes;
  int num_cells;
  gridvalue_t *grid_data;
} hypergrid;

/** Creates a hypergrid with the given number of dimensions and dimension lengths
*/
hypergrid* hypergrid_create(int dim, int *sizes);

/** Copies an existing hypergrid
*/
hypergrid* hypergrid_copy(hypergrid *srcgrid);

/** Deallocates a hypergrid
*/
void hypergrid_free(hypergrid *gridptr);

/** Creates an coords that can be used to access the hypergrid's elements
*/
int* hypergrid_coords_create(hypergrid *gridptr);

/** Deallocates a hypergrid coords
*/
void hypergrid_coords_free(int *coords);

/** Returns true if the given coords is valid for the given hypergrid
(i.e. all components are less than the size of the hypergrid's corresponding dimension)
*/
int  hypergrid_coords_isvalid(hypergrid *gridptr, int *coords);

/** Increments the coords for the given hypergrid, returns true if successful,
0 if not (i.e. if the coords is at the maximum possible position). If the
coords is invalid the results are undefined.
*/
int hypergrid_coords_increment(hypergrid *gridptr, int *coords);

/** Gets the value at the given coords
*/
gridvalue_t hypergrid_get_value(hypergrid *gridptr, int *coords);

/** Sets the value at the given coords
*/
void hypergrid_set_value(hypergrid *gridptr, int *coords, gridvalue_t value);

/** Returns the size of the given dimension
*/
int hypergrid_dimension_size(hypergrid *gridptr, int dim);

/** Returns the number of neighbors of the given coords which have non-zero values.
*/
/* cmpequal: true to count cells equal to cmpval, false to count cells not equal */
/* wrap: true to wrap around edges of grid */
int hypergrid_neighbor_count(hypergrid *gridptr, int *coords, int cmpequal, gridvalue_t cmpval, int wrap);

/** Returns the number of cells in the entire grid with value equal to cmpval
*/
int hypergrid_value_count(hypergrid *gridptr, int cmpval);

/** Returns true if any cells have a value equal to cmpval
*/
int hypergrid_value_exists(hypergrid *gridptr, int cmpval);

/** Returns true if the two hypergrids have equal dimension sizes
*/
int hypergrid_equal_dimensions(hypergrid *gridptr1, hypergrid *gridptr2);

/** Returns true if the two hypergrids have equal dimensions and contents
*/
int hypergrid_equal(hypergrid *gridptr1, hypergrid *gridptr2);

void* hypergrid_archive(hypergrid *gridptr, int *size);

hypergrid* hypergrid_unarchive(void *data);


int** hypergrid_neighbors(hypergrid *gridptr, int *centercoords, int distance, int *count);

void hypergrid_coords_list_free(int **list, int count);

/** Adds the components of two arrays and returns a new array. The caller must free
the returned array.
*/
int* array_add(int *arr1, int *arr2, int count);

/** Subtracts the components of two arrays and returns a new array. The caller must free
the returned array.
*/
int* array_subtract(int *arr1, int *arr2, int count);

// debugging...

void hypergrid_debug_coords(hypergrid *gridptr, int *coords);
