//
//  AnyDirectionUntilBlockedMoveRule.h
//  Gridlock
//
//  Created by Brian on Sat Nov 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Game.h"

@interface AnyDirectionUntilBlockedMoveRule : NSObject {

}

+(NSArray *)allValidMoveSequences:(Game *)game fromPositionsWithValue:(int)sval;
+(NSArray *)allValidMoveSequences:(Game *)game;

+(BOOL)gameHasAnyMoves:(Game *)game fromPositionsWithValue:(int)sval;
+(BOOL)gameHasAnyMoves:(Game *)game;

@end
