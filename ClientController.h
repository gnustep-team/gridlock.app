/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import <AppKit/AppKit.h>
#import "AccessorMacros.h"
#import "NetConnection.h"
#import "GameConfiguration.h"

@interface ClientController : NSObject
{
    IBOutlet id chatField;
    IBOutlet id connectionStatusField;
    IBOutlet id gameInfoField;
    IBOutlet id hostField;
    IBOutlet id hostnameCheckbox;
    IBOutlet id usernameField;
    IBOutlet id passwordField;
    IBOutlet id portField;
    IBOutlet id rendezvousCheckbox;
    IBOutlet id rendezvousHostsPopup;
    IBOutlet id windowSizerField;
    IBOutlet id window;

    BOOL isWindowExpanded;
    float windowHeightDelta;

    NSSize windowDisconnectedSize;
    NSSize windowConnectedSize;
    NSFileHandle *socket;
    NetConnection *netConnection;
    GameConfiguration *gameConfiguration;

    id rendezvousBrowser;
    NSMutableArray *rendezvousServers;

    int connectionIDCounter;
    int waitingForConnectionID;
}

idAccessor_h(socket, setSocket)
idAccessor_h(netConnection, setNetConnection)
idAccessor_h(gameConfiguration, setGameConfiguration)
idAccessor_h(rendezvousBrowser, setRendezvousBrowser)

- (IBAction)connect:(id)sender;
- (IBAction)connectMethodChanged:(id)sender;
- (IBAction)disconnect:(id)sender;
- (IBAction)rendezvousHostSelected:(id)sender;
- (IBAction)startGame:(id)sender;
- (IBAction)updateUsername:(id)sender;

-(void)showWindow;
-(void)setWindowExpanded:(BOOL)exp display:(BOOL)disp;

// private methods
-(void)setUseRendezvousServer:(BOOL)value;
-(void)setRendezvousEnabled:(BOOL)value;
-(void)setupRendezvousDiscovery;
-(void)sendConnectMessage;

@end
