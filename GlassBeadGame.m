/* Gridlock
Copyright (c) 2002-2003 by Brian Nenninger. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#import "GlassBeadGame.h"

/** Cells with actual pieces have values corresponding to player numbers (+1 and +2). Cells
which don't have pieces but are controlled by a player have the negative of the player
numbers (-1 and -2).
*/

@implementation GlassBeadGame

-(void)reset {
  [super reset];
  
  [self setGrid:[DCHypergrid gridWithRows:[[[self configurationInfo] objectForKey:@"rows"] intValue]
                                  columns:[[[self configurationInfo] objectForKey:@"cols"] intValue]]];
}

-(BOOL)prepareMoveSequence:(NSArray *)positions {
  DCHypergridPosition *pos = [positions lastObject];
  NSArray *neighbors = [[self grid] neighborsOfPosition:pos distance:1];
  NSEnumerator *me = [neighbors objectEnumerator];
  DCHypergridPosition *mpos;
  int pnum = [self currentPlayerNumber];
  int oppnum = [self nextPlayerNumber];
  
  [self resetFutureGrid];

  [[self futureGrid] setValue:pnum atPosition:pos];
  // check each neighbor of this move for changes
  while (mpos=[me nextObject]) {
    // only check cells that don't have pieces on them
    if ([self valueAtPosition:mpos]<=0 && ![mpos isEqual:pos]) {
      // count current neighbors (of each neighbor to the proposed move) for each players' pieces
      int mycount = 0;
      int oppcount = 0;
      int dr, dc;
      for(dr=-1; dr<=1; dr++) {
        int r = [mpos row]+dr;
        if (r>=0 && r<[self numberOfRows]) {
          for(dc=-1; dc<=1; dc++) {
            int c = [mpos column]+dc;
            if (c>=0 && c<[self numberOfColumns]) {
              int value = [self valueAtRow:r column:c];
              if (value==pnum) ++mycount;
              else if (value==oppnum) ++oppcount;
            }
          }
        }
      }

      if (mycount==oppcount) {
        // empty cell will become owned by current player
        [[self futureGrid] setValue:-pnum atPosition:mpos];
      }
      else if (mycount+1==oppcount) {
        // cell owned by opponent will become empty
        [[self futureGrid] setValue:0 atPosition:mpos];
      }
    }
  }
  return YES;
}

-(int)scoreForPlayer:(int)pnum {
  // sum of # of pieces and # of controlled cells
  return [[self grid] numberOfCellsWithValue:pnum] +
         [[self grid] numberOfCellsWithValue:0-pnum];
}

-(BOOL)isGameOver {
  // game over when all cells are filled or controlled
  return (![[self grid] hasCellWithValue:0]);
}

-(NSArray *)allValidMoveSequences {
  return [[[self grid] allPositionsWithValue:0] valuesByObjectsPerformingSelector:@selector(arrayWithSelf_)];
}

-(BOOL)showScores {
  return YES;
}

@end
